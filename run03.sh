#!/bin/bash

# make hw03.out

#epss=("0.05" "0.1" "0.2" "0.5" "0.8")
#sady=("NK" "ZKC" "ZKW")
#ns=("4" "10" "15" "20" "22" "25" "27" "30" "32" "35" "37" "40")
#
#for eps in ${epss[*]} ; do
#  for sada in ${sady[*]} ; do
#    for n in ${ns[*]} ; do
#      date; echo "${sada}/${sada}${n}_inst.dat"; cat "./${sada}/${sada}${n}_inst.dat" | ./hw02.out "fptas" $eps >> "./out2_3_${sada}_fptas_${eps}.csv"
#    done
#  done
#done

BASE_PATH="/home/vanisond/fit/zs20/KOP"
REPORT_PATH="${BASE_PATH}/report"
GEN_PATH="${BASE_PATH}/gen"
HW_PATH="${BASE_PATH}/hw"
OUT_PATH="./output"

# Change path to HW folder
cd "${HW_PATH}"

# Create output directory
mkdir -p "${OUT_PATH}"

# Make binary
make hw03.out

for W in 10 20000 50000 ; do
  "${GEN_PATH}/kg2" -n 30 -N 100 -W "${W}" -C 9000 > "${OUT_PATH}/W${W}_inst.dat"
  for method in "bb" "greedy" "redux" "fptas" "dp_cost" ; do
    # date; echo "W=${W} ${method}"; cat "W${W}_inst.dat" | ../hw/hw03.out "${method}" >> "./out_W${W}_${method}.csv"
    echo "W=${W} ${method}";
    cat "${OUT_PATH}/W${W}_inst.dat" | "${HW_PATH}/hw03.out" "${method}" > "${OUT_PATH}/out_W${W}_${method}.csv"
  done
done
