#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <limits>
#include <string>
#include <vector>

using namespace std;

struct Instance 
{
    // Vstupní proměnné
    int id;
    unsigned int n; // počet položek
    int bagCapacity = -888;
    int requiredMinCost = -888; // pro rozhodovací verzi
    int maxPossibleSumCost = 0; // součet cen položek v batohu; maximální možná cena, pokud zahrnu všechny položky v batohu
    int maxItemCost = 0; // nejdražší položka v batohu
    int maxPossibleWeight = 0; // maximální možná váha (součet všech položek)
    vector<int> weights;
    vector<int> costs;
    // Výstupní proměnné
    bool hasSolution = false;
    vector<bool> solution;
    int finalWeight = 0;
    int finalCost = 0;
};

template<class T> ostream& operator<< (ostream& os, const vector<T>& vec) {
    os << "[";
    for(size_t i = 0; i < vec.size(); ++i) {
        if(i > 0) { os << ", "; }
        os << setw(4) << vec[i];
    }
    os << "]";
    return os;
}

void print_partial_state(const vector<bool> & vec, unsigned int len) {
    cout << "[";
    for(size_t i = 0; i < len; ++i) {
        cout << vec[i];
    }
    for(size_t i = len; i < vec.size(); ++i) {
        cout << " ";
    }
    cout << "]";
}

void print_solution(const Instance& ins) {
    cout << "Has solution: ";
    print_partial_state(ins.solution, ins.n);
    cout << endl;
    cout << "weight: " << ins.finalWeight << endl;
    cout << "cost: " << ins.finalCost << endl;
}

ostream& operator<<(ostream& os, const Instance& ins) {
    return os << "(" << ins.id << ")"
              << " n=" << ins.n
              << " M=" << ins.bagCapacity
              << " C=" << ins.requiredMinCost
              << "\n"
              << ins.weights
              << "\n"
              << ins.costs
              << endl
              << ins.solution
              << endl
              << "cost:" << ins.finalCost
              << endl
              << "wright:" << ins.finalWeight
              << endl;
}
