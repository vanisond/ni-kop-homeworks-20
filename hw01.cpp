#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <limits>
#include <string>
#include <vector>

#include "./utils.hpp"

using namespace std;

unsigned long long int g_counter = 0;

vector<Instance> instances = {};

void load_data(std::istream &is) {
    int id;
    while(is >> id) {
        Instance ins = {id};
        is >> ins.n >> ins.bagCapacity;
        // zaporna ID predstavuji rozhodovaci verzi
        if(id < 0) {
            is >> ins.requiredMinCost;
        }
        for(size_t i = 0; i < ins.n; ++i) {
            int weight, cost;
            is >> weight >> cost;
            ins.weights.push_back(weight);
            ins.costs.push_back(cost);
            ins.maxPossibleSumCost += cost;
            ins.maxPossibleWeight += weight;
        }
        instances.push_back(ins);
    }
}

// Brute force reseni 
void f_bf(unsigned int index, vector<bool>& items, int weight, int cost, Instance& ins) {
    g_counter++;

    // koncová podmínka
    if(index == ins.n) {
        // cout << setw(3) << g_counter << " ";
        // cout << "i = " << index << " ";
        // print_partial_state(items, index);
        // cout << endl;
        // cout << "weight: " << weight << endl;
        // cout << "cost: " << cost << endl;

        if(weight > ins.bagCapacity) {
            ins.hasSolution = false;
            return;
        }
        if(cost < ins.requiredMinCost) {
            ins.hasSolution = false;
            return;
        }
        ins.hasSolution = true;
        ins.solution = items;
        ins.finalCost = cost;
        ins.finalWeight = weight;
        return;
    }

    // add current item
    items[index] = 1;
    f_bf(index+1, items, weight + ins.weights[index], cost + ins.costs[index], ins);
    if(ins.hasSolution) {
        return;
    }
    // don't add item
    items[index] = 0;
    f_bf(index+1, items, weight, cost, ins);
}




void f_bb(unsigned int index, vector<bool>& items, int weight, int cost, int remainingCost, Instance& ins) {
    g_counter++;

    // print_partial_state(items, index);
    // cerr << "{"<<g_counter<<", i:"<<setw(2)<<index<<"} "<< remainingCost << " >= " << ins.requiredMinCost
    //      << ", has:" << ins.hasSolution
    //      << ", cost:" << cost
    //      << ", item:" << ins.costs[index] << endl;

    // překročil jsem maximální povolenou kapacitu batohu
    if(weight > ins.bagCapacity) {
        // cerr << "[1] weight > ins.bagCapacity" << endl;
        // ins.hasSolution = false;
        return;
    }

    // kdykoliv jsem se dostal do stavu, který splnil podmínky pro řešení, můžu skončit
    if(cost >= ins.requiredMinCost) {
        ins.hasSolution = true;
        ins.solution = items;
        ins.finalCost = cost;
        ins.finalWeight = weight;
        // cerr << "Mam rešeni" << ins << endl;
        // cerr << items << endl;
        // cerr << cost << endl;
        // cerr << weight << endl;
        return;
    }

    // Pomocí zbývajících položek již nemůžu najít lepší řešení
    if(ins.requiredMinCost > cost + remainingCost) {
        // cerr << "[2] ins.requiredMinCost < remainingCost" << endl;
        ins.hasSolution = false;
        return;
    }

    // koncová podmínka - již jsem prošel všechny položky
    if(index == ins.n) {
        return;
    }

    // add current item
    items[index] = 1;
    f_bb(index+1, items, weight + ins.weights[index], cost + ins.costs[index], remainingCost - ins.costs[index], ins);
    if(ins.hasSolution) {
        return;
    }
    // don't add item
    items[index] = 0;
    f_bb(index+1, items, weight, cost, remainingCost - ins.costs[index], ins);
}



void solve(Instance& ins, const string & method) {
    ins.hasSolution = false; // reset
    vector<bool> items(ins.n, 0);
    if ("bb" == method)
    {
        f_bb(0, items, 0, 0, ins.maxPossibleSumCost, ins);
    } else if ("bf" == method)
    {
        f_bf(0, items, 0, 0, ins);
    }
}


int main(int argc, char* argv[]) {
    if(argc != 2) {
        cout << "Usage: ";
        cout << argv[0] << " <method>" << endl;
        cout << "method: \"bf\" (brute force), \"bb\" (branch and bounce)" << endl;
        return 1;
    }
    unsigned long repeat_factor = 1;
    string method = argv[1]; // metoda: "bb", "bf"
    load_data(cin);

    cout << "method; ID; n; has_solution; requiredMinCost; finalCost; maxPossibleSumCost; bag_capacity; used_capacity; maxPossibleWeight; solution_configuration; time (s); counter; repeat_factor" << endl;
    for(auto & ins : instances) {
        cerr << "."; // progress indicator (print one dot for each instance)
        g_counter = 0; // reset global counter
        chrono::steady_clock::time_point _start(chrono::steady_clock::now());
        solve(ins, method);
        // přepočítej repeat_factor na základě prvního běhu
        repeat_factor = 1 + 33554432 / g_counter;
        if (repeat_factor > 1048576)
        {
            repeat_factor = 1048576;
        }
        
        for(size_t i = 1; i < repeat_factor; i++) {
            solve(ins, method);
        }
        chrono::steady_clock::time_point _end(chrono::steady_clock::now());

        cout << method << "; " ;
        cout << ins.id << "; " ;
        cout << ins.n << "; " ;
        cout << (ins.hasSolution ? "A" : "N") << "; " ;
        cout << (ins.requiredMinCost) << "; " ;
        cout << (ins.finalCost) << "; " ;
        cout << (ins.maxPossibleSumCost) << "; " ;
        cout << (ins.bagCapacity) << "; " ;
        cout << (ins.finalWeight) << "; " ;
        cout << (ins.maxPossibleWeight) << "; " ;
        if(!ins.hasSolution) {
            for (size_t i=0; i<ins.n; i++) {
                if(i>0) cout << " ";
                cout << 0;
            }
        } else {
            int checkSolution = 0;
            for (size_t i=0; i<ins.n; i++) {
                if(i>0) cout << " ";
                cout << ins.solution[i];
                if(ins.solution[i]) {
                    checkSolution += ins.costs[i];
                }
            }

            // kontrola výsledku
            if (checkSolution != ins.finalCost)
            {
                cout << endl << "[ERR] " << checkSolution << " != " << ins.finalCost << endl << endl;
            }
            if ((ins.finalCost >= ins.requiredMinCost) ^ ins.hasSolution) {
                cout << endl << "[ERR] " << ins.finalCost << " < " << ins.requiredMinCost << endl;
                cout << endl << "[ERR] " << " != " << ins.hasSolution << endl << endl;
            }
        }
        cout << "; ";
        cout << fixed << chrono::duration_cast<chrono::duration<double>>(_end - _start).count();
        cout << "; ";
        cout << g_counter;
        cout << "; ";
        cout << repeat_factor;
        cout << endl;
    }
    return 0;
}

