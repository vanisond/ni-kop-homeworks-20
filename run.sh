#!/bin/bash

make hw01.out

# ####################################################################

# Run entire dataset and safe outup to csv
# cat ./NR/NR4_inst.dat | ./hw01.out 20000 > ./NR4_inst_out.csv

# Run custom instance
# echo "-1 4 46 324 36 3 43 1129 202 94 149 2084" | ./hw01.out 20000

# Run one line from dataset
# head -n 1 ZR/ZR40_inst.dat | ./hw01.out "bf"

# Run in parallel
# tail NR/NR30_inst.dat -n +1   | head -n 100 | ./hw01.out "bf" >> ./NR30_inst_out.csv &
# tail NR/NR30_inst.dat -n +101 | head -n 100 | ./hw01.out "bf" >> ./NR30_inst_out.csv &
# tail NR/NR30_inst.dat -n +201 | head -n 100 | ./hw01.out "bf" >> ./NR30_inst_out.csv &
# tail NR/NR30_inst.dat -n +301 | head -n 100 | ./hw01.out "bf" >> ./NR30_inst_out.csv &
# tail NR/NR30_inst.dat -n +401 | head -n 100 | ./hw01.out "bf" >> ./NR30_inst_out.csv 

# cat NR/NR30_inst.dat | ./hw01.out "bf" >> ./NR30_inst_out.2.csv

# ####################################################################


# METHOD="bf" # "bb" (branch and bounce), "bf" (brute force)
# FILES="./N?/N?*_inst.dat"
# for f in $FILES
# do
#     date
#     echo "[INFO] Processing $f using $METHOD method..."
#     cat $f | ./hw01.out "$METHOD" >> "./out_$METHOD.csv"
# done
# echo "\\n\\n" >> "./out_$METHOD.csv"

# date

# ####################################################################

#
#date; echo "NR/NR4_inst.dat"; head "./NR/NR4_inst.dat" | ./hw02.out "bf" >> "./out2_3_NR_bf.csv"
#date; echo "NR/NR10_inst.dat"; head "./NR/NR10_inst.dat" | ./hw02.out "bf" >> "./out2_3_NR_bf.csv"
#date; echo "NR/NR15_inst.dat"; head "./NR/NR15_inst.dat" | ./hw02.out "bf" >> "./out2_3_NR_bf.csv"
#date; echo "NR/NR20_inst.dat"; head "./NR/NR20_inst.dat" | ./hw02.out "bf" >> "./out2_3_NR_bf.csv"
#date; echo "NR/NR22_inst.dat"; head "./NR/NR22_inst.dat" | ./hw02.out "bf" >> "./out2_3_NR_bf.csv"
#date; echo "NR/NR25_inst.dat"; head "./NR/NR25_inst.dat" | ./hw02.out "bf" >> "./out2_3_NR_bf.csv"
#date; echo "NR/NR27_inst.dat"; head "./NR/NR27_inst.dat" | ./hw02.out "bf" >> "./out2_3_NR_bf.csv"
#date; echo "NR/NR30_inst.dat"; head "./NR/NR30_inst.dat" | ./hw02.out "bf" >> "./out2_3_NR_bf.csv"
#date; echo "NR/NR32_inst.dat"; head "./NR/NR32_inst.dat" | ./hw02.out "bf" >> "./out2_3_NR_bf.csv"
#
#date; echo "ZKC/ZKC4_inst.dat"; head "./ZKC/ZKC4_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKC_bf.csv"
#date; echo "ZKC/ZKC10_inst.dat"; head "./ZKC/ZKC10_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKC_bf.csv"
#date; echo "ZKC/ZKC15_inst.dat"; head "./ZKC/ZKC15_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKC_bf.csv"
#date; echo "ZKC/ZKC20_inst.dat"; head "./ZKC/ZKC20_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKC_bf.csv"
#date; echo "ZKC/ZKC22_inst.dat"; head "./ZKC/ZKC22_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKC_bf.csv"
#date; echo "ZKC/ZKC25_inst.dat"; head "./ZKC/ZKC25_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKC_bf.csv"
#date; echo "ZKC/ZKC27_inst.dat"; head "./ZKC/ZKC27_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKC_bf.csv"
#date; echo "ZKC/ZKC30_inst.dat"; head "./ZKC/ZKC30_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKC_bf.csv"
#date; echo "ZKC/ZKC32_inst.dat"; head "./ZKC/ZKC32_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKC_bf.csv"
#
#date; echo "ZKW/ZKW4_inst.dat"; head "./ZKW/ZKW4_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKW_bf.csv"
#date; echo "ZKW/ZKW10_inst.dat"; head "./ZKW/ZKW10_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKW_bf.csv"
#date; echo "ZKW/ZKW15_inst.dat"; head "./ZKW/ZKW15_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKW_bf.csv"
#date; echo "ZKW/ZKW20_inst.dat"; head "./ZKW/ZKW20_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKW_bf.csv"
#date; echo "ZKW/ZKW22_inst.dat"; head "./ZKW/ZKW22_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKW_bf.csv"
#date; echo "ZKW/ZKW25_inst.dat"; head "./ZKW/ZKW25_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKW_bf.csv"
#date; echo "ZKW/ZKW27_inst.dat"; head "./ZKW/ZKW27_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKW_bf.csv"
#date; echo "ZKW/ZKW30_inst.dat"; head "./ZKW/ZKW30_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKW_bf.csv"
#date; echo "ZKW/ZKW32_inst.dat"; head "./ZKW/ZKW32_inst.dat" | ./hw02.out "bf" >> "./out2_3_ZKW_bf.csv"


#date; echo "NR/NR4_inst.dat"; cat "./NR/NR4_inst.dat" | ./hw02.out "bb" >> "./out2_3_NR_bb.csv"
#date; echo "NR/NR10_inst.dat"; cat "./NR/NR10_inst.dat" | ./hw02.out "bb" >> "./out2_3_NR_bb.csv"
#date; echo "NR/NR15_inst.dat"; cat "./NR/NR15_inst.dat" | ./hw02.out "bb" >> "./out2_3_NR_bb.csv"
#date; echo "NR/NR20_inst.dat"; cat "./NR/NR20_inst.dat" | ./hw02.out "bb" >> "./out2_3_NR_bb.csv"
#date; echo "NR/NR22_inst.dat"; cat "./NR/NR22_inst.dat" | ./hw02.out "bb" >> "./out2_3_NR_bb.csv"
#date; echo "NR/NR25_inst.dat"; cat "./NR/NR25_inst.dat" | ./hw02.out "bb" >> "./out2_3_NR_bb.csv"
#date; echo "NR/NR27_inst.dat"; cat "./NR/NR27_inst.dat" | ./hw02.out "bb" >> "./out2_3_NR_bb.csv"
#date; echo "NR/NR30_inst.dat"; cat "./NR/NR30_inst.dat" | ./hw02.out "bb" >> "./out2_3_NR_bb.csv"
#date; echo "NR/NR32_inst.dat"; cat "./NR/NR32_inst.dat" | ./hw02.out "bb" >> "./out2_3_NR_bb.csv"
#
#date; echo "ZKC/ZKC4_inst.dat"; cat "./ZKC/ZKC4_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKC_bb.csv"
#date; echo "ZKC/ZKC10_inst.dat"; cat "./ZKC/ZKC10_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKC_bb.csv"
#date; echo "ZKC/ZKC15_inst.dat"; cat "./ZKC/ZKC15_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKC_bb.csv"
#date; echo "ZKC/ZKC20_inst.dat"; cat "./ZKC/ZKC20_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKC_bb.csv"
#date; echo "ZKC/ZKC22_inst.dat"; cat "./ZKC/ZKC22_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKC_bb.csv"
#date; echo "ZKC/ZKC25_inst.dat"; cat "./ZKC/ZKC25_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKC_bb.csv"
#date; echo "ZKC/ZKC27_inst.dat"; cat "./ZKC/ZKC27_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKC_bb.csv"
#date; echo "ZKC/ZKC30_inst.dat"; cat "./ZKC/ZKC30_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKC_bb.csv"
#date; echo "ZKC/ZKC32_inst.dat"; cat "./ZKC/ZKC32_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKC_bb.csv"
#
#date; echo "ZKW/ZKW4_inst.dat"; cat "./ZKW/ZKW4_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKW_bb.csv"
#date; echo "ZKW/ZKW10_inst.dat"; cat "./ZKW/ZKW10_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKW_bb.csv"
#date; echo "ZKW/ZKW15_inst.dat"; cat "./ZKW/ZKW15_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKW_bb.csv"
#date; echo "ZKW/ZKW20_inst.dat"; cat "./ZKW/ZKW20_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKW_bb.csv"
#date; echo "ZKW/ZKW22_inst.dat"; cat "./ZKW/ZKW22_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKW_bb.csv"
#date; echo "ZKW/ZKW25_inst.dat"; cat "./ZKW/ZKW25_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKW_bb.csv"
#date; echo "ZKW/ZKW27_inst.dat"; cat "./ZKW/ZKW27_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKW_bb.csv"
#date; echo "ZKW/ZKW30_inst.dat"; cat "./ZKW/ZKW30_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKW_bb.csv"
#date; echo "ZKW/ZKW32_inst.dat"; cat "./ZKW/ZKW32_inst.dat" | ./hw02.out "bb" >> "./out2_3_ZKW_bb.csv"
#
#
#date; echo "NR/NR4_inst.dat"; cat "./NR/NR4_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_NR_dp_cost.csv"
#date; echo "NR/NR10_inst.dat"; cat "./NR/NR10_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_NR_dp_cost.csv"
#date; echo "NR/NR15_inst.dat"; cat "./NR/NR15_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_NR_dp_cost.csv"
#date; echo "NR/NR20_inst.dat"; cat "./NR/NR20_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_NR_dp_cost.csv"
#date; echo "NR/NR22_inst.dat"; cat "./NR/NR22_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_NR_dp_cost.csv"
#date; echo "NR/NR25_inst.dat"; cat "./NR/NR25_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_NR_dp_cost.csv"
#date; echo "NR/NR27_inst.dat"; cat "./NR/NR27_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_NR_dp_cost.csv"
#date; echo "NR/NR30_inst.dat"; cat "./NR/NR30_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_NR_dp_cost.csv"
#date; echo "NR/NR32_inst.dat"; cat "./NR/NR32_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_NR_dp_cost.csv"
#
#date; echo "ZKC/ZKC4_inst.dat"; cat "./ZKC/ZKC4_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKC_dp_cost.csv"
#date; echo "ZKC/ZKC10_inst.dat"; cat "./ZKC/ZKC10_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKC_dp_cost.csv"
#date; echo "ZKC/ZKC15_inst.dat"; cat "./ZKC/ZKC15_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKC_dp_cost.csv"
#date; echo "ZKC/ZKC20_inst.dat"; cat "./ZKC/ZKC20_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKC_dp_cost.csv"
#date; echo "ZKC/ZKC22_inst.dat"; cat "./ZKC/ZKC22_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKC_dp_cost.csv"
#date; echo "ZKC/ZKC25_inst.dat"; cat "./ZKC/ZKC25_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKC_dp_cost.csv"
#date; echo "ZKC/ZKC27_inst.dat"; cat "./ZKC/ZKC27_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKC_dp_cost.csv"
#date; echo "ZKC/ZKC30_inst.dat"; cat "./ZKC/ZKC30_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKC_dp_cost.csv"
#date; echo "ZKC/ZKC32_inst.dat"; cat "./ZKC/ZKC32_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKC_dp_cost.csv"
#
#date; echo "ZKW/ZKW4_inst.dat"; cat "./ZKW/ZKW4_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKW_dp_cost.csv"
#date; echo "ZKW/ZKW10_inst.dat"; cat "./ZKW/ZKW10_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKW_dp_cost.csv"
#date; echo "ZKW/ZKW15_inst.dat"; cat "./ZKW/ZKW15_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKW_dp_cost.csv"
#date; echo "ZKW/ZKW20_inst.dat"; cat "./ZKW/ZKW20_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKW_dp_cost.csv"
#date; echo "ZKW/ZKW22_inst.dat"; cat "./ZKW/ZKW22_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKW_dp_cost.csv"
#date; echo "ZKW/ZKW25_inst.dat"; cat "./ZKW/ZKW25_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKW_dp_cost.csv"
#date; echo "ZKW/ZKW27_inst.dat"; cat "./ZKW/ZKW27_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKW_dp_cost.csv"
#date; echo "ZKW/ZKW30_inst.dat"; cat "./ZKW/ZKW30_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKW_dp_cost.csv"
#date; echo "ZKW/ZKW32_inst.dat"; cat "./ZKW/ZKW32_inst.dat" | ./hw02.out "dp_cost" >> "./out2_3_ZKW_dp_cost.csv"
#
#
#date; echo "NR/NR4_inst.dat"; cat "./NR/NR4_inst.dat" | ./hw02.out "greedy" >> "./out2_3_NR_greedy.csv"
#date; echo "NR/NR10_inst.dat"; cat "./NR/NR10_inst.dat" | ./hw02.out "greedy" >> "./out2_3_NR_greedy.csv"
#date; echo "NR/NR15_inst.dat"; cat "./NR/NR15_inst.dat" | ./hw02.out "greedy" >> "./out2_3_NR_greedy.csv"
#date; echo "NR/NR20_inst.dat"; cat "./NR/NR20_inst.dat" | ./hw02.out "greedy" >> "./out2_3_NR_greedy.csv"
#date; echo "NR/NR22_inst.dat"; cat "./NR/NR22_inst.dat" | ./hw02.out "greedy" >> "./out2_3_NR_greedy.csv"
#date; echo "NR/NR25_inst.dat"; cat "./NR/NR25_inst.dat" | ./hw02.out "greedy" >> "./out2_3_NR_greedy.csv"
#date; echo "NR/NR27_inst.dat"; cat "./NR/NR27_inst.dat" | ./hw02.out "greedy" >> "./out2_3_NR_greedy.csv"
#date; echo "NR/NR30_inst.dat"; cat "./NR/NR30_inst.dat" | ./hw02.out "greedy" >> "./out2_3_NR_greedy.csv"
#date; echo "NR/NR32_inst.dat"; cat "./NR/NR32_inst.dat" | ./hw02.out "greedy" >> "./out2_3_NR_greedy.csv"
#
#date; echo "ZKC/ZKC4_inst.dat"; cat "./ZKC/ZKC4_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKC_greedy.csv"
#date; echo "ZKC/ZKC10_inst.dat"; cat "./ZKC/ZKC10_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKC_greedy.csv"
#date; echo "ZKC/ZKC15_inst.dat"; cat "./ZKC/ZKC15_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKC_greedy.csv"
#date; echo "ZKC/ZKC20_inst.dat"; cat "./ZKC/ZKC20_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKC_greedy.csv"
#date; echo "ZKC/ZKC22_inst.dat"; cat "./ZKC/ZKC22_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKC_greedy.csv"
#date; echo "ZKC/ZKC25_inst.dat"; cat "./ZKC/ZKC25_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKC_greedy.csv"
#date; echo "ZKC/ZKC27_inst.dat"; cat "./ZKC/ZKC27_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKC_greedy.csv"
#date; echo "ZKC/ZKC30_inst.dat"; cat "./ZKC/ZKC30_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKC_greedy.csv"
#date; echo "ZKC/ZKC32_inst.dat"; cat "./ZKC/ZKC32_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKC_greedy.csv"
#
#date; echo "ZKW/ZKW4_inst.dat"; cat "./ZKW/ZKW4_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKW_greedy.csv"
#date; echo "ZKW/ZKW10_inst.dat"; cat "./ZKW/ZKW10_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKW_greedy.csv"
#date; echo "ZKW/ZKW15_inst.dat"; cat "./ZKW/ZKW15_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKW_greedy.csv"
#date; echo "ZKW/ZKW20_inst.dat"; cat "./ZKW/ZKW20_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKW_greedy.csv"
#date; echo "ZKW/ZKW22_inst.dat"; cat "./ZKW/ZKW22_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKW_greedy.csv"
#date; echo "ZKW/ZKW25_inst.dat"; cat "./ZKW/ZKW25_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKW_greedy.csv"
#date; echo "ZKW/ZKW27_inst.dat"; cat "./ZKW/ZKW27_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKW_greedy.csv"
#date; echo "ZKW/ZKW30_inst.dat"; cat "./ZKW/ZKW30_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKW_greedy.csv"
#date; echo "ZKW/ZKW32_inst.dat"; cat "./ZKW/ZKW32_inst.dat" | ./hw02.out "greedy" >> "./out2_3_ZKW_greedy.csv"
#
#
#date; echo "NR/NR4_inst.dat"; cat "./NR/NR4_inst.dat" | ./hw02.out "redux" >> "./out2_3_NR_redux.csv"
#date; echo "NR/NR10_inst.dat"; cat "./NR/NR10_inst.dat" | ./hw02.out "redux" >> "./out2_3_NR_redux.csv"
#date; echo "NR/NR15_inst.dat"; cat "./NR/NR15_inst.dat" | ./hw02.out "redux" >> "./out2_3_NR_redux.csv"
#date; echo "NR/NR20_inst.dat"; cat "./NR/NR20_inst.dat" | ./hw02.out "redux" >> "./out2_3_NR_redux.csv"
#date; echo "NR/NR22_inst.dat"; cat "./NR/NR22_inst.dat" | ./hw02.out "redux" >> "./out2_3_NR_redux.csv"
#date; echo "NR/NR25_inst.dat"; cat "./NR/NR25_inst.dat" | ./hw02.out "redux" >> "./out2_3_NR_redux.csv"
#date; echo "NR/NR27_inst.dat"; cat "./NR/NR27_inst.dat" | ./hw02.out "redux" >> "./out2_3_NR_redux.csv"
#date; echo "NR/NR30_inst.dat"; cat "./NR/NR30_inst.dat" | ./hw02.out "redux" >> "./out2_3_NR_redux.csv"
#date; echo "NR/NR32_inst.dat"; cat "./NR/NR32_inst.dat" | ./hw02.out "redux" >> "./out2_3_NR_redux.csv"
#
#date; echo "ZKC/ZKC4_inst.dat"; cat "./ZKC/ZKC4_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKC_redux.csv"
#date; echo "ZKC/ZKC10_inst.dat"; cat "./ZKC/ZKC10_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKC_redux.csv"
#date; echo "ZKC/ZKC15_inst.dat"; cat "./ZKC/ZKC15_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKC_redux.csv"
#date; echo "ZKC/ZKC20_inst.dat"; cat "./ZKC/ZKC20_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKC_redux.csv"
#date; echo "ZKC/ZKC22_inst.dat"; cat "./ZKC/ZKC22_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKC_redux.csv"
#date; echo "ZKC/ZKC25_inst.dat"; cat "./ZKC/ZKC25_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKC_redux.csv"
#date; echo "ZKC/ZKC27_inst.dat"; cat "./ZKC/ZKC27_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKC_redux.csv"
#date; echo "ZKC/ZKC30_inst.dat"; cat "./ZKC/ZKC30_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKC_redux.csv"
#date; echo "ZKC/ZKC32_inst.dat"; cat "./ZKC/ZKC32_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKC_redux.csv"
#
#date; echo "ZKW/ZKW4_inst.dat"; cat "./ZKW/ZKW4_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKW_redux.csv"
#date; echo "ZKW/ZKW10_inst.dat"; cat "./ZKW/ZKW10_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKW_redux.csv"
#date; echo "ZKW/ZKW15_inst.dat"; cat "./ZKW/ZKW15_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKW_redux.csv"
#date; echo "ZKW/ZKW20_inst.dat"; cat "./ZKW/ZKW20_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKW_redux.csv"
#date; echo "ZKW/ZKW22_inst.dat"; cat "./ZKW/ZKW22_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKW_redux.csv"
#date; echo "ZKW/ZKW25_inst.dat"; cat "./ZKW/ZKW25_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKW_redux.csv"
#date; echo "ZKW/ZKW27_inst.dat"; cat "./ZKW/ZKW27_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKW_redux.csv"
#date; echo "ZKW/ZKW30_inst.dat"; cat "./ZKW/ZKW30_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKW_redux.csv"
#date; echo "ZKW/ZKW32_inst.dat"; cat "./ZKW/ZKW32_inst.dat" | ./hw02.out "redux" >> "./out2_3_ZKW_redux.csv"


epss=("0.05" "0.1" "0.2" "0.5" "0.8")
sady=("NK" "ZKC" "ZKW")
ns=("4" "10" "15" "20" "22" "25" "27" "30" "32" "35" "37" "40")

for eps in ${epss[*]} ; do
  for sada in ${sady[*]} ; do
    for n in ${ns[*]} ; do
      date; echo "${sada}/${sada}${n}_inst.dat"; cat "./${sada}/${sada}${n}_inst.dat" | ./hw02.out "fptas" $eps >> "./out2_3_${sada}_fptas_${eps}.csv"
    done
  done
done
