CC     = g++
CFLAGS = -std=c++17 -g -Wall -pedantic -O3

all: default
default: hw01.out hw02.out hw03.out

hw01.out:hw01.cpp
	${CC} ${CFLAGS} -o hw01.out hw01.cpp

hw02.out:hw02.cpp
	${CC} ${CFLAGS} -o hw02.out hw02.cpp

hw03.out:hw03.cpp
	${CC} ${CFLAGS} -o hw03.out hw03.cpp

clean:
	rm hw01.out hw02.out hw03.out

.PHONY: default clean
