#!/bin/bash

sady=("NK" "ZKC" "ZKW")
ns=("4" "10" "15" "20" "22" "25" "27" "30" "32" "35" "37" "40")

for sada in ${sady[*]} ; do
  for n in ${ns[*]} ; do
    echo "${sada}/${sada}${n}_sol.dat"
    # prumerny soucet polozek
    awk '{for(i=3;i<=NF;i+=2)total+=$i} END {print "CsumAVG: ",total/NR}' "${sada}/${sada}${n}_inst.dat"
    # maximalni soucet cen polozek
    awk '{m=$3;for(i=3;i<=NF;i+=2)m=m+$i;print "Csum: ",m}' "${sada}/${sada}${n}_inst.dat" | sort -n | tail -1
    # maximalni cena polozek napric vsemi instancemi
    awk '{n=$2;m=$3;for(i=3;i<=NF;i+=2)if($i>m)m=$i;print "Cmax: ",m}' "${sada}/${sada}${n}_inst.dat" | sort -n | tail -1
    # max ze vsech minimálních relativních chyb
    awk '{n=$2;m=$3;for(i=3;i<=NF;i+=2)if($i>m)m=$i;print "eps: ",n/m}' "${sada}/${sada}${n}_inst.dat" | sort -n | tail -1
  done
done
