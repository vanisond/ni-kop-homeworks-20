#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <limits>
#include <cmath>
#include <string>
#include <vector>
#include <tuple>

#include "./utils.hpp"

#define MY_INFINITY 1073741824
#define DEBUG false

using namespace std;

unsigned long long int g_counter = 0;

vector<Instance> instances = {};

void load_data(std::istream &is) {
    int id;
    while (is >> id) {
        Instance ins = {id};
        is >> ins.n >> ins.bagCapacity;
        // zaporna ID predstavuji rozhodovaci verzi
        if (id < 0) {
            is >> ins.requiredMinCost;
        }
        for (size_t i = 0; i < ins.n; ++i) {
            int weight, cost;
            is >> weight >> cost;
            ins.weights.push_back(weight);
            ins.costs.push_back(cost);
            ins.maxPossibleSumCost += cost;
            ins.maxPossibleWeight += weight;
            if (cost > ins.maxItemCost) {
                ins.maxItemCost = cost;
            }
        }
        instances.push_back(ins);
    }
}

// Brute force reseni 
void f_bf(unsigned int index, vector<bool> &items, int weight, int cost, Instance &ins) {
    g_counter++;

    // koncová podmínka
    if (index == ins.n) {
        if (weight > ins.bagCapacity) {
            return;
        }
        if (cost < ins.requiredMinCost) {
            return;
        }

        if (cost >= ins.finalCost) {
            ins.hasSolution = true;
            ins.solution = std::vector<bool>(items);
            ins.finalCost = cost;
            ins.finalWeight = weight;
        }

        return;
    }

    // add current item
    items[index] = 1;
    f_bf(index + 1, items, weight + ins.weights[index], cost + ins.costs[index], ins);

    // don't add item
    items[index] = 0;
    f_bf(index + 1, items, weight, cost, ins);
}


// Branch and bound reseni
void f_bb(unsigned int index, vector<bool> &items, int weight, int cost, int remainingCost, Instance &ins) {
    g_counter++;

    // překročil jsem maximální povolenou kapacitu batohu
    if (weight > ins.bagCapacity) {
        return;
    }

    // Pomocí zbývajících položek již nemůžu najít lepší řešení
    if (ins.finalCost > cost + remainingCost) {
        return;
    }

    // koncová podmínka - již jsem prošel všechny položky
    if (index == ins.n) {
        // pokud jsem našel lepší řešení než doposud nejlepší -> aktualizuji řešení
        if (cost >= ins.finalCost) {
            ins.hasSolution = true;
            ins.solution = std::vector<bool>(items);
            ins.finalCost = cost;
            ins.finalWeight = weight;
        }
        return;
    }

    // add current item
    items[index] = 1;
    f_bb(index + 1, items, weight + ins.weights[index], cost + ins.costs[index], remainingCost - ins.costs[index], ins);

    // don't add item
    items[index] = 0;
    f_bb(index + 1, items, weight, cost, remainingCost - ins.costs[index], ins);
}

// Dynamicke programovani - dekompozice podle ceny
void f_dp_cost(Instance &ins) {
    // MOCK DATA
//    vector<int> weights = {2, 6, 5, 3, 4};
//    vector<int> costs = {5, 9, 20, 12, 18};
//    unsigned int c_max = 64;
//    ins.maxPossibleSumCost = c_max;
//    ins.n = 5;
//    ins.bagCapacity = 10;
//    ins.costs = costs;
//    ins.weights = weights;
//    ins.bagCapacity = 10;
    // ---------

    // Memoziovaná tabulka vah
    vector<vector<int>> W(ins.n + 1, vector<int>(ins.maxPossibleSumCost + 1, INT16_MAX - ins.maxPossibleSumCost));

    // 1) W(0,0) = 0
    W[0][0] = 0;

    for (unsigned int i = 0; i <= ins.n; ++i) {
        for (int c = 0; c <= ins.maxPossibleSumCost; ++c) {

            // 2) W(0,c) = ∞ pro všechna c > 0 -> tj. první sloupec bez spodní položky je +inf
            if (i == 0 && c > 0) {
                W[i][c] = INT16_MAX - ins.maxPossibleSumCost; // todo: int_max
                continue;
            }


            // 3) W(i+1, c) = min(W(i, c), W(i, c-c_{i+1}) + w_{i+1}) pro všechna i > 0.
            if (i > 0) {

                // Ošetření případu, kdy bych vyšel mimo tabulku - vezmu hodnotu "vlevo"
                if (c - ins.costs[i - 1] < 0) {
                    W[i][c] = W[i - 1][c];
                    continue;
                }

                W[i][c] = min(
                    W[i - 1][c],
                    W[i - 1][c - ins.costs[i - 1]] + ins.weights[i - 1]
                );
            }
        }
    }

    // print table
#if DEBUG
    cout << endl;
    for (int c = ins.maxPossibleSumCost; c >= 0; --c) {
        bool skip = true;
        for (unsigned int i = 0; i <= ins.n; ++i) {
            if (W[i][c] != INT32_MAX - ins.maxPossibleSumCost) {
                skip = false;
                break;
            }
        }
        if (skip) {
            continue;
        }
        cout << setw(2) << c;
        for (unsigned int i = 0; i <= ins.n; ++i) {
            cout << setw(22) << W[i][c];
        }
        cout << endl;
    }
    cout << "n:";
    for (unsigned int i = 0; i <= ins.n; ++i) {
        cout << setw(22) << i;
    }
    cout << "\nW:" << setw(22) << 0;
    for (unsigned int i = 0; i < ins.n; ++i) {
        cout << setw(22) << ins.weights[i];
    }
    cout << "\nC:" << setw(22) << 0;
    for (unsigned int i = 0; i < ins.n; ++i) {
        cout << setw(22) << ins.costs[i];
    }
    cout << endl;
#endif

    // 2 . část -> nalezení nejlepší váhy v posledním sloupci tabulky, která se vejde do kapacity batohu
    auto rit = std::find_if(W[ins.n].rbegin(), W[ins.n].rend(), [&ins](int w) { return w <= ins.bagCapacity; });
    if (rit != W[ins.n].rend()) {
        size_t c = std::distance(begin(W[ins.n]), rit.base()) - 1;
        ins.finalCost = c;
        ins.finalWeight = W[ins.n][c];
#if DEBUG
        std::cout << "cost=" << ins.finalCost << ", weight=" << ins.finalWeight << endl;
#endif
        ins.solution = vector<bool>(ins.n, false);

        for (unsigned int i = ins.n; i > 0; --i) {
#if DEBUG
            cout << "W(" << i << "," << c << ") = " << W[i][c] << ", ";
            cout << "W(" << i - 1 << "," << c << ") = " << W[i - 1][c] << ", ";
            cout << "W(" << i - 1 << "," << c - ins.costs[i - 1] << ") = " << W[i - 1][c - ins.costs[i - 1]] << endl;
            cout << "W(" << i - 1 << "," << c - ins.costs[i - 1] << ") + " << ins.weights[i-1] << " = "
                 << (W[i - 1][c - ins.costs[i - 1]] + ins.weights[i-1]) << " ?= " << W[i][c] << endl;
#endif
            if (W[i][c] == W[i - 1][c]) {
                ins.solution[i - 1] = false;
            } else {
                ins.solution[i - 1] = true;
                c = c - ins.costs[i - 1];
            }
#if DEBUG
            cout << i << ". " << ins.solution << endl << endl;
#endif
        }

    } else {
        cerr << "[ERR] id:" << ins.id << endl;
        ins.solution = vector<bool>(ins.n, false);
    }
}


// FPTAS
void f_fptas(double eps, Instance &ins) {
    // MOCK DATA
//    vector<int> weights = {2, 6, 5, 3, 4};
//    vector<int> costs = {5, 9, 20, 12, 18};
//    int c_max = 64;
//    ins.maxPossibleSumCost = c_max;
//    ins.maxItemCost = *max_element(std::begin(costs), std::end(costs));
//    ins.n = 5;
//    ins.bagCapacity = 10;
//    ins.costs = costs;
//    ins.weights = weights;
//    ins.bagCapacity = 10;
    // ---------

    //Uložení původních cen položek pro přepočet výsledné ceny
    auto costs = ins.costs;

    //Přepočítání cen položek podle zadaného eps
    double adjustFactor = (eps * ins.maxItemCost) / ins.n;
    if (adjustFactor < 1) {
        cerr << "[WARN] eps = " << eps << " ---> eps > " << ((double) ins.n / ins.maxItemCost) << endl;
        adjustFactor = 1;
    }
//    cerr << "max item cost: " << ins.maxItemCost << endl;
//    cerr << "adjustFactor: " << adjustFactor << endl;
//    cerr << "maxPossibleSumCost: " << ins.maxPossibleSumCost << endl;
    ins.maxPossibleSumCost = floor(ins.maxPossibleSumCost / adjustFactor);
//    cerr << "maxPossibleSumCost: " << ins.maxPossibleSumCost << endl;
    for (std::vector<int>::size_type i = 0; i != ins.costs.size(); i++) {
        ins.costs[i] = floor(ins.costs[i] / adjustFactor);
    }

    // Vyřešení úlohy pomocí DP
    f_dp_cost(ins);

    // Přepočítání výsledné ceny
    ins.finalCost = 0;
    ins.costs = costs;
    for (uint i = 0; i < ins.n; ++i) {
        if (ins.solution[i]) {
            ins.finalCost += costs[i];
        }
    }
}

void f_greedy(Instance &ins) {
    struct Item {
        size_t index;
        int weight;
        int cost;
    };
    vector<Item> items = {};
    items.reserve(ins.n);
    for (std::size_t i = 0; i < ins.n; ++i) {
        items.push_back({i, ins.weights[i], ins.costs[i]});
    }
    std::sort(items.begin(), items.end(), [](const Item &a, const Item &b) -> bool {
        return ((a.cost / (double) a.weight) > (b.cost / (double) b.weight));
    });
    ins.finalWeight = 0;
    ins.solution = vector<bool>(ins.n, false);
//    cout << ">>> capacity: " << ins.bagCapacity << ", C: " << ins.finalCost << ", W: " << ins.finalWeight << endl;
    for (auto &item : items) {
//        cout << setw(4) << (pair.cost / (double) pair.weight) << ", ";
        if (ins.finalWeight + item.weight <= ins.bagCapacity) {
            ins.finalWeight += item.weight;
            ins.finalCost += item.cost;
            ins.solution[item.index] = true;
        }
//        cout << item.index << ": " << item.cost << ", " << item.weight << " => " << (item.cost / (double) item.weight);
//        cout << "| capacity: " << ins.bagCapacity << ", C: " << ins.finalCost << ", W: " << ins.finalWeight << endl;
    }
}

void f_redux(Instance &ins) {
    // 1) Use greedy heuristics
    f_greedy(ins);

    // 2) Find the most valuable item
    int bestCost = 0;
    uint itemIndex = -1;
    for (uint i = 0; i < ins.n; ++i) {
        if (ins.weights[i] > ins.bagCapacity) {
            continue;
        }
        if (ins.costs[i] > bestCost) {
            bestCost = ins.costs[i];
            itemIndex = i;
        }
    }
    // bestCost is equal to ins.costs[itemIndex]

    // 3) compare the most valuable item to the greedy solution; optionally update solution
    if (bestCost > ins.finalCost) {
//        cerr << "[REDUX] " << ins.solution;
        ins.finalCost = bestCost;
        ins.finalWeight = ins.weights[itemIndex];
        ins.solution = vector<bool>(ins.n, false);
        ins.solution[itemIndex] = true;
//        cerr << " < " << ins.solution << endl;
    }
}


void solve(Instance &ins, const string &method, double eps = 0) {
    ins.hasSolution = false; // reset
    if ("bb" == method) {
        vector<bool> items(ins.n, false);
        f_bb(0, items, 0, 0, ins.maxPossibleSumCost, ins);
    } else if ("bf" == method) {
        vector<bool> items(ins.n, false);
        f_bf(0, items, 0, 0, ins);
    } else if ("dp_cost" == method) {
        f_dp_cost(ins);
    } else if ("fptas" == method) {
        f_fptas(eps, ins);

    } else if ("greedy" == method) {
        f_greedy(ins);
    } else if ("redux" == method) {
        f_redux(ins);
    }
}


int main(int argc, char *argv[]) {
    if (argc < 2) {
        cout << "Usage: ";
        cout << argv[0] << " <method> <eps>" << endl;
        cout << "method: \"bf\" (brute force), \"bb\" (branch and bounce)" << endl;
        cout << "eps: relativni chyba fptas" << endl;
        return 1;
    }
    unsigned long repeat_factor = 1;
    string method = argv[1]; // metoda: "bb", "bf"
    double eps = 0.05; // přesnost epsylon
    if (argc > 2) {
        sscanf(argv[2], "%lf", &eps);
//        cout << "reading " << argv[2] << " = " << eps << endl;
    }

    load_data(cin);
//
//    cout
//            << "method; ID; n; has_solution; requiredMinCost; finalCost; maxPossibleSumCost; bag_capacity; used_capacity; maxPossibleWeight; solution_configuration; time (s); counter; repeat_factor"
//            << endl;

    for (auto &ins : instances) {
        cerr << "."; // progress indicator (print one dot for each instance)
        g_counter = 0; // reset global counter
        chrono::steady_clock::time_point _start(chrono::steady_clock::now());
//        auto ins_copy = ins;
        solve(ins, method, eps);

//        chrono::steady_clock::time_point _endFirst(chrono::steady_clock::now());
//        double firstRunTime = chrono::duration_cast<chrono::duration<double>>(_endFirst - _start).count();
//        if (firstRunTime < 0.32) {
//            repeat_factor = (uint) (0.16 / firstRunTime);
//            for (uint i = 0; i <= repeat_factor; ++i) {
//                ins_copy = ins;
//                solve(ins_copy, method, eps);
//            }
//        }

        chrono::steady_clock::time_point _end(chrono::steady_clock::now());

        //todo: set solution
        ins.hasSolution = ins.finalCost > 0;

        cout << method << "; ";
        cout << ins.id << "; ";
        cout << ins.n << "; ";
        cout << (ins.hasSolution ? "A" : "N") << "; ";
        cout << (ins.requiredMinCost) << "; ";
        cout << (ins.finalCost) << "; ";
        cout << (ins.maxPossibleSumCost) << "; ";
        cout << (ins.bagCapacity) << "; ";
        cout << (ins.finalWeight) << "; ";
        cout << (ins.maxPossibleWeight) << "; ";
        if (!ins.hasSolution) {
            for (size_t i = 0; i < ins.n; i++) {
                if (i > 0) cout << " ";
                cout << 0;
            }
        } else {
            int checkSolution = 0;
            for (size_t i = 0; i < ins.n; i++) {
                if (i > 0) cout << " ";
                cout << ins.solution[i];
                if (ins.solution[i]) {
                    checkSolution += ins.costs[i];
                }
            }

            // kontrola výsledku
            if (checkSolution != ins.finalCost) {
                // Pro aproximativni algoritmy zakomentovat
                // cout << endl << "[ERR] " << checkSolution << " != " << ins.finalCost << endl << endl;
            }
            if ((ins.finalCost >= ins.requiredMinCost) ^ ins.hasSolution) {
//                cout << endl << "[ERR] " << ins.finalCost << " < " << ins.requiredMinCost << endl;
//                cout << endl << "[ERR] " << " != " << ins.hasSolution << endl << endl;
            }
        }
        cout << "; ";
        cout << fixed << chrono::duration_cast<chrono::duration<double>>(_end - _start).count();
        cout << "; ";
        cout << g_counter;
        cout << "; ";
        cout << repeat_factor;
        cout << endl;
    }
    return 0;
}

